import React from 'react'
import { Card, Button, Icon } from 'semantic-ui-react'
import {Link} from "react-router-dom";

const extra = (movie_id,deleteMovie) => {
    return (
        <div className="ui two buttons">
            <Button animated='vertical' as={Link} to={`/movies/edit/${movie_id}/`}>
                <Button.Content hidden>Edit</Button.Content>
                <Button.Content visible>
                    <Icon name='right arrow'/>
                </Button.Content>
            </Button>
            <Button animated='vertical' onClick={() => deleteMovie(movie_id)}>
                <Button.Content hidden>Delete</Button.Content>
                <Button.Content visible>
                    <Icon name='trash'/>
                </Button.Content>
            </Button>
        </div>)
};

const MovieCard = ({ movie, deleteMovie }) => (

    <Card
        header={ movie.title }
        image='https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg'
        meta='Friend'
        extra={extra(movie.id, deleteMovie)}
        description='Elliot is a sound engineer living in Nashville who enjoys playing guitar and hanging with his cat.'
    />
);

export default MovieCard