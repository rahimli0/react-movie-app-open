import React, {Component} from 'react';
import { Form, Image } from 'semantic-ui-react'
import { Segment } from 'semantic-ui-react'
import InlineError from "./InlineError";
import PropTypes from 'prop-types';
import MessageNegative from "./NegativeMessage";
import { Redirect } from 'react-router-dom'



class NewMovieForm extends Component {
    state = {
        id: this.props.movie ? this.props.movie.id :null,
        title: this.props.movie ? this.props.movie.title :'',
        thumbnailUrl:this.props.movie ? 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg' :'',
        errors:[],
        redirect:false
    };
    handleChange = (e) => {
      this.setState({
          [e.target.name] : e.target.value
      })
    };
    onSubmit = () => {
        const  errors = this.validate();
        this.setState({
            errors,
            redirect:true
        });
        const id = this.state.id || this.props.newMovie.movie.id;
        // console.log(id);
        // return false;
        if (Object.keys(errors).length === 0)
            this.props.onNewMovieSubmit(this.state)
        else
            this.props.onUpdateMovieSubmit({ ...this.state,id})
    };
    validate = () => {
        const  errors = {};
        if (!this.state.title) errors.title = "title can not blank";
        if (!this.state.thumbnailUrl) errors.thumbnailUrl = "thumbnailUrl can not blank";
        return errors
    };
    componentWillReceiveProps(nextProps, nextContext) {
        const { movie } = nextProps.newMovie;
        if (movie.title && movie.title !== this.state.title){
            this.setState({
                title:movie.title,
                thumbnailUrl:movie.title ? 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg' :'',
            })
        }
    }

    render() {
        const errors = this.state.errors;
        const form = (
            <Segment attached>
                <Form loading={this.props.newMovie.fetching || this.props.newMovie.movie.fetching}>
                    <Form.Input fluid label='Title' error={!!errors.title} placeholder='Title' id="title" name="title" value={this.state.title} onChange={this.handleChange}/>
                    {errors.title && <InlineError message={errors.title}></InlineError> }
                    <Form.Input fluid label='thumbnailUrl' error={!!errors.title} placeholder='thumbnailUrl' name="thumbnailUrl" id="thumbnailUrl" value={this.state.thumbnailUrl}  onChange={this.handleChange}/>
                    {errors.thumbnailUrl && <InlineError message={errors.thumbnailUrl}></InlineError> }
                    <Image className="clearfix" src={this.state.thumbnailUrl} size='medium' bordered />
                    <Form.Button onClick={this.onSubmit} primary>Submit</Form.Button>

                    {
                        this.props.newMovie.error && <MessageNegative title="error"></MessageNegative>
                    }

                </Form>
            </Segment>
        );
        return (
            <div>
                {
                    this.props.newMovie.done && this.state.redirect ? <Redirect to="/movies/"></Redirect> :  form
                }
            </div>
        );
    }
}

NewMovieForm.propTypes = {
    onNewMovieSubmit: PropTypes.func.isRequired,
    onUpdateMovieSubmit: PropTypes.func.isRequired
};

export default NewMovieForm;