import React from 'react'
import { Message } from 'semantic-ui-react'

const MessageNegative = ({title}) => (
    <Message negative>
        <Message.Header>{title}</Message.Header>
    </Message>
)

export default MessageNegative