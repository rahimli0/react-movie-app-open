import React from 'react';
import PropTypes from 'prop-types';
import MovieCard from "./parts/MovieCard";
import { Grid  } from 'semantic-ui-react'
import ScaleLoader from "react-spinners/ScaleLoader";
const MoviesList = ({ movies, deleteMovie }) => {
    const emptyMessage = (
        <p>There are no movies yet.</p>
    );

    const moviesList = (
        <div>

            <ScaleLoader
                size={150}
                color={"#bc0a00"}
                loading={movies.fetching}
            />
            { movies.error ? <h3>Error retirirving data</h3> :
                <Grid stackable columns={3}>
                    { movies.movieList.map(movie => <Grid.Column  key={movie.id}>
                        <MovieCard movie={movie} deleteMovie={deleteMovie}>{movie.title}<hr/> </MovieCard>
                    </Grid.Column>) }
                </Grid>
            }
        </div>
    );

    return (
        <div>

            { movies.length === 0 ? emptyMessage : moviesList }
        </div>
    );
};

MoviesList.propTypes = {
    movies: PropTypes.shape({
        movieList: PropTypes.array.isRequired
    }).isRequired
};

export default MoviesList;