import React, {Component} from 'react';
import {connect} from "react-redux";
import NewMovieForm from '../parts/NewMovieForm'
import {onNewMovieSubmit,onUpdateMovieSubmit,fetchMovie} from "../../actions/newMovie";
import {Header} from "semantic-ui-react";


class MoviesNewPage extends Component {
    componentDidMount() {
        const { match } = this.props;
        if (!this.props.movie && match.params.id){
            this.props.fetchMovie(match.params.id);
        }
    }

    render() {
        return (
            <div>
                <Header as='h2' attached='top'>
                    New Movie
                </Header>
                <NewMovieForm
                    movie={this.props.movie}
                    onNewMovieSubmit={this.props.onNewMovieSubmit}
                    onUpdateMovieSubmit={this.props.onUpdateMovieSubmit}
                    newMovie={this.props.newMovie}/>
            </div>
        );
    }
}

const mapStateToProps = ({ newMovie, movies }, props) => {
    return {
        newMovie ,
        movie: movies.movieList.find(item => item.id === props.match.params.id),
    }
};

const mapDispatchToProps = {
    onNewMovieSubmit,
    onUpdateMovieSubmit,
    fetchMovie
};

export default connect(mapStateToProps,mapDispatchToProps)(MoviesNewPage);