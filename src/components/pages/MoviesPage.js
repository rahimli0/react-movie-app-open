import React, {Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import MoviesList from "../MoviesList";

import {fetchMovies, deleteMovie} from '../../actions/movies'



class MoviesPage extends Component {

    static propTypes = {
        movies: PropTypes.object.isRequired
    };

    state = {

    };
    componentDidMount() {
        this.props.fetchMovies();
    }

    render() {
        console.log(this.props);
        return (
            <div>
                <h2>Movies</h2>

                <MoviesList movies={this.props.movies} deleteMovie={this.props.deleteMovie}></MoviesList>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        movies: state.movies,
        deleteMovie: PropTypes.func.isRequired
    }
};

const  mapDispatchToProps = {
    fetchMovies,
    deleteMovie
};

export default connect(mapStateToProps,mapDispatchToProps)(MoviesPage);