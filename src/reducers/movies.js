import {FETCHED_MOVIES_FULFILLED, FETCHED_MOVIES_REJECTED, FETCHED_MOVIES_PENDING,
    DELETE_MOVIE_FULFILLED, DELETE_MOVIE_REJECTED, DELETE_MOVIE_PENDING,
} from '../actions/movies'

const initialState = {
    fetching: false,
    fetched: false,
    movieList: [],
    movie :{
        error:''
    }
};

export default (state=initialState, action) => {
    switch (action.type) {
        case  FETCHED_MOVIES_PENDING:
            return {
                ...state,
                fetching:true,
            };
        case  FETCHED_MOVIES_FULFILLED:
            return {
                ...state,
                movieList:action.payload,
                fetching:false,
            };
        case  FETCHED_MOVIES_REJECTED:
            return {
                ...state,
                error:action.payload,
                fetching:false,
            };
        case  DELETE_MOVIE_PENDING:
            return {
                ...state,
            };
        case  DELETE_MOVIE_FULFILLED:
            return {
                ...state,
                movieList:state.movieList.filter(item => item.id !== action.payload.id),
            };
        case  DELETE_MOVIE_REJECTED:
            return {
                ...state,
                // error:action.payload,
            };
        default:
            return state
    }
}