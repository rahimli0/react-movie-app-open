import {
    NEW_MOVIE_FULFILLED, NEW_MOVIE_REJECTED, NEW_MOVIE_PENDING,
    UPDATE_MOVIE_FULFILLED, UPDATE_MOVIE_REJECTED, UPDATE_MOVIE_PENDING,
    FETCHED_MOVIE_FULFILLED, FETCHED_MOVIE_REJECTED, FETCHED_MOVIE_PENDING} from '../actions/newMovie'

const initialState = {
    fetching: false,
    fetched: false,
    done: false,
    movie: {
        fetching: false,
    }
};

export default (state=initialState, action) => {
    switch (action.type) {
        case  NEW_MOVIE_PENDING:
            return {
                ...state,
                fetching:true,
                done:false,
            };
        case  NEW_MOVIE_FULFILLED:
            return {
                ...state,
                done:true,
                fetching:false,
                movies: action.payload
            };
        case  NEW_MOVIE_REJECTED:
            return {
                ...state,
                // error:action.payload,
                fetching:false,
                done:false,
            };

        case  UPDATE_MOVIE_PENDING:
            return {
                ...state,
                fetching:true,
                done:false,
            };
        case  UPDATE_MOVIE_FULFILLED:
            return {
                ...state,
                done:true,
                fetching:false,
                movies: action.payload
            };
        case  UPDATE_MOVIE_REJECTED:
            return {
                ...state,
                // error:action.payload,
                fetching:false,
                done:false,
            };

        // case  DELETE_MOVIE_PENDING:
        //     return {
        //         ...state,
        //         fetching:true,
        //         done:false,
        //     };
        // case  DELETE_MOVIE_FULFILLED:
        //     return {
        //         ...state,
        //         done:true,
        //         fetching:false,
        //         movies: action.payload
        //     };
        // case  DELETE_MOVIE_REJECTED:
        //     return {
        //         ...state,
        //         // error:action.payload,
        //         fetching:false,
        //         done:false,
        //     };



        case  FETCHED_MOVIE_PENDING:
            return {
                ...state,
                movie:{
                    fetching:true
                },
                // fetching:true,
                // done:false,
            };
        case  FETCHED_MOVIE_FULFILLED:
            return {
                ...state,
                movie:{
                    ...action.payload,
                    fetching:false
                },
                // done:true,
                // fetching:false,
            };
        case  FETCHED_MOVIE_REJECTED:
            return {
                ...state,
                movie:{
                    fetching:false
                },
                // error:action.payload,
                // fetching:false,
                // done:false,
            };
        default:
            return state
    }
}