import axios from 'axios'
import { API_BASE_URL, API_BASE_TOKEN } from '../config/env'

export const NEW_MOVIE_PENDING = "NEW_MOVIE_PENDING";
export const NEW_MOVIE_FULFILLED = "NEW_MOVIE_FULFILLED";
export const NEW_MOVIE_REJECTED = "NEW_MOVIE_REJECTED";

export const UPDATE_MOVIE_PENDING = "UPDATE_MOVIE_PENDING";
export const UPDATE_MOVIE_FULFILLED = "UPDATE_MOVIE_FULFILLED";
export const UPDATE_MOVIE_REJECTED = "UPDATE_MOVIE_REJECTED";

export const FETCHED_MOVIE_PENDING = "FETCHED_MOVIE_PENDING";
export const FETCHED_MOVIE_FULFILLED = "FETCHED_MOVIE_FULFILLED";
export const FETCHED_MOVIE_REJECTED = "FETCHED_MOVIE_REJECTED";

export function onNewMovieSubmit({title,thumbnailUrl} ) {
    return dispatch => {
        dispatch({
            type:"NEW_MOVIE",
            payload:axios.post(`https://jsonplaceholder.typicode.com/albums/1/photos`,
                {
                    'title':title,
                    'url':'https://via.placeholder.com/600/24f355',
                    'thumbnailUrl':thumbnailUrl
                }
            ).then(
                result =>console.log(result)
            )
        })
    }
}

export function onUpdateMovieSubmit({id, title,thumbnailUrl} ) {
    return dispatch => {
        dispatch({
            type:"UPDATE_MOVIE",
            payload:axios.put(`https://jsonplaceholder.typicode.com/albums/1/photos/${id}`,
                {
                    'title':title,
                    'url':'https://via.placeholder.com/600/24f355',
                    'thumbnailUrl':thumbnailUrl
                }
            ).then(
                result =>console.log(result)
            )
        })
    }
}


export function onDeleteMovieSubmit({id, title,thumbnailUrl} ) {
    return dispatch => {
        dispatch({
            type:"DELETE_MOVIE",
            payload:axios.delete(`https://jsonplaceholder.typicode.com/albums/1/photos/${id}`,).then(
                result =>console.log(result)
            )
        })
    }
}
export function fetchMovie(id) {
    return dispatch => {
        dispatch({
            type:"FETCHED_MOVIE",
            payload:axios.get(`${API_BASE_URL}posts/${id}?_format=json&access-token=${API_BASE_TOKEN}`).then(
                result => result.data.result
            )
        })
    }
}