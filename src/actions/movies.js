import axios from 'axios'
import { API_BASE_URL, API_BASE_TOKEN } from '../config/env'

export const FETCHED_MOVIES_PENDING = "FETCHED_MOVIES_PENDING";
export const FETCHED_MOVIES_FULFILLED = "FETCHED_MOVIES_FULFILLED";
export const FETCHED_MOVIES_REJECTED = "FETCHED_MOVIES_REJECTED";
export function fetchMovies() {
    return dispatch => {
        dispatch({
            type:"FETCHED_MOVIES",
            payload:axios.get(`${API_BASE_URL}posts?_format=json&access-token=${API_BASE_TOKEN}`).then(
                result =>result.data.result
            )
        })
    }
}

export const DELETE_MOVIE_PENDING = "DELETE_MOVIE_PENDING";
export const DELETE_MOVIE_FULFILLED = "DELETE_MOVIE_FULFILLED";
export const DELETE_MOVIE_REJECTED = "DELETE_MOVIE_REJECTED";

export function deleteMovie({id} ) {
    return dispatch => {
        dispatch({
            type:"DELETE_MOVIE",
            payload:axios.delete(`https://jsonplaceholder.typicode.com/albums/1/photos/${id}`,).then(
                result => Object.assign({}, result, { id })
            )
        })
    }
}